# About the project

**Food Clicker** is an example for [Android Kotlin Fundamentals: codelabs 04.1-04.2](https://codelabs.developers.google.com/codelabs/kotlin-android-training-lifecycles-logging/index.html). But instead of having one image to click on, there's a 5x5 grid of different images, and the player has to click only those which appear in a separate area of 3 "selected" images. The score increases for clicking on correct images, and decreases for clicking on incorrect images.

The 3 selected images are refreshed every 5 sec, and every image on the grid which stays longer than 10 sec is also refreshed. This is done with the timer, which was introduced in the original Dessert Clicker, but it wasn't used for anything except logging. Maybe it's not the correct way to implement a game loop, but at least it makes the timer useful.

The layout is a Constraint Layout, built semi-manually with TOML templates and a Python script. (See [util](util/).) There's also a separate layout for the landscape orientation.

The images are from [Free Match 3 game assets on itch.io](https://free-game-assets.itch.io/free-match-3-game-assets). It's entirely possible to use the .xml images from the original Dessert Clicker, but it looks as if they include a lot of internal padding, so they appear rather small on the grid, and I couldn't figure out how to get rid of the padding.

The apk is here: [foodclicker.apk](apk/foodclicker.apk).

___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)



