/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.catness.foodclicker

import android.content.ActivityNotFoundException
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import androidx.databinding.DataBindingUtil
import org.catness.foodclicker.databinding.ActivityMainBinding
import timber.log.Timber

/** onSaveInstanceState Bundle Keys **/
const val KEY_SCORE = "score_key"
const val KEY_TIMER_SECONDS = "timer_seconds_key"

class MainActivity : AppCompatActivity() {

    private var revenue = 0
    private var dessertsSold = 0
    private var score = 0

    private lateinit var dessertTimer : DessertTimer;

    // Contains all the views
    private lateinit var binding: ActivityMainBinding

    /** Dessert Data **/

    /**
     * Simple data class that represents a dessert. Includes the resource id integer associated with
     * the image, the price it's sold for, and the startProductionAmount, which determines when
     * the dessert starts to be produced.
     */

    data class Cell(var widget: ImageView, var seconds: Int)

    // Create a list of all desserts, in order of when they start being produced
    // It remained from the original example
    /* 
    data class Dessert(val imageId: Int)
    
    private val allDesserts = listOf(
            Dessert(R.drawable.cupcake, 5, 0),
            Dessert(R.drawable.donut, 10, 5),
            Dessert(R.drawable.eclair, 15, 20),
            Dessert(R.drawable.froyo, 30, 50),
            Dessert(R.drawable.gingerbread, 50, 100),
            Dessert(R.drawable.honeycomb, 100, 200),
            Dessert(R.drawable.icecreamsandwich, 500, 500),
            Dessert(R.drawable.jellybean, 1000, 1000),
            Dessert(R.drawable.kitkat, 2000, 2000),
            Dessert(R.drawable.lollipop, 3000, 4000),
            Dessert(R.drawable.marshmallow, 4000, 8000),
            Dessert(R.drawable.nougat, 5000, 16000),
            Dessert(R.drawable.oreo, 6000, 20000)
    )
*/

    private val allDesserts = listOf(
        R.drawable.a1,
        R.drawable.a2,
        R.drawable.a3,
        R.drawable.a4,
        R.drawable.a5,
        R.drawable.a6,
        R.drawable.a7,
        R.drawable.a8,
        R.drawable.a9,
        R.drawable.a10,

        R.drawable.a11,
        R.drawable.a12,
        R.drawable.a13,
        R.drawable.a14,
        R.drawable.a15,
        R.drawable.a16,
        R.drawable.a17,
        R.drawable.a18,
        R.drawable.a19,
        R.drawable.a20,
        
        R.drawable.a21
    )


    private lateinit var grid: HashMap<ImageView, Int>
    private lateinit var select: List<ImageView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.i("onCreate called")

        // Use Data Binding to get reference to the views
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.apply {
            grid = hashMapOf(box1 to 0, box2 to 0, box3 to 0, box4 to 0, box5 to 0,
                             box6 to 0, box7 to 0, box8 to 0, box9 to 0, box10 to 0,
                             box11 to 0, box12 to 0, box13 to 0, box14 to 0, box15 to 0,
                             box16 to 0, box17 to 0, box18 to 0, box19 to 0, box20 to 0,
                             box21 to 0, box22 to 0, box23 to 0, box24 to 0, box25 to 0)
            select = listOf(select1, select2, select3)
        }

        randomizeGrid()

        // Setup dessertTimer, passing in the lifecycle
        dessertTimer = DessertTimer(this.lifecycle, this)

        // If there is a savedInstanceState bundle, then you're "restarting" the activity
        // If there isn't a bundle, then it's a "fresh" start
        if (savedInstanceState != null) {
            score = savedInstanceState.getInt(KEY_SCORE, 0)
            dessertTimer.secondsCount =
                    savedInstanceState.getInt(KEY_TIMER_SECONDS, 0)
        }

        // Set the TextViews to the right values
        binding.seconds = 0
        binding.score = score
    }

    private fun randomizeSelected() {
        select.forEach {
            var imageId = allDesserts.random()
            it.setImageResource(imageId)
            it.setTag(imageId)
        }       
    }


    private fun randomizeGrid() {
        for ((widget, value) in grid) {
            var imageId = allDesserts.random()
            widget.setImageResource(imageId)
            widget.setTag(imageId)
            widget.setOnClickListener {
                v -> onDessertClicked(v as ImageView)
            }
        }
        randomizeSelected()
    }

    /**
     * Updates the score when the dessert is clicked. Shows a new dessert.
     */
    private fun onDessertClicked(widget: ImageView) {
        // We can't compare getDrawable() directly - the values are different even if the images are the same.
        // So we compare the tags, set to the imageId,

        var image = widget.getTag()
        // Timber.i("got image: $image")
        var add = 0
        select.forEach {
            // Timber.i("select: drawable="+it.getTag())
            if (it.getTag() == image) add++
        }
        // Timber.i("add=$add")
        if (add==0) score--
        else score+=add

        var imageId = allDesserts.random()
        widget.setImageResource(imageId)
        widget.setTag(imageId)
        grid[widget] = 0 // reset seconds to 0

        binding.score = score
    }


    fun updateSeconds() {

        // reset the "select" images every 5 sec
        if (dessertTimer.secondsCount%5 == 0) {
            randomizeSelected()
        }

        // reset all the images which remain for more than 10 sec to different random images
        for ((widget, value) in grid) {
            if (value > 10) {
                var imageId = allDesserts.random()
                widget.setImageResource(imageId)
                widget.setTag(imageId)
                grid[widget] = 0
            }
            else {
                grid[widget] = value+1   // grid[widget]++ doesn't compile
            }
        }
    }

 
    /**
     * Menu methods
     */
    private fun onShare() {
        val shareIntent = ShareCompat.IntentBuilder.from(this)
                .setText(getString(R.string.share_text, dessertTimer.secondsCount, score))
                .setType("text/plain")
                .intent
        try {
            startActivity(shareIntent)
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(this, getString(R.string.sharing_not_available),
                    Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.shareMenuButton -> onShare()
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Called when the user navigates away from the app but might come back
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Timber.i("onSaveInstanceState Called")
        outState.putInt(KEY_SCORE, score)
        outState.putInt(KEY_TIMER_SECONDS, dessertTimer.secondsCount)
    }

    /** Lifecycle Methods **/
    override fun onStart() {
        super.onStart()

        Timber.i("onStart called")
    }

    override fun onResume() {
        super.onResume()
        Timber.i("onResume Called")
    }

    override fun onPause() {
        super.onPause()
        Timber.i("onPause Called")
    }

    override fun onStop() {
        super.onStop()
        Timber.i("onStop Called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.i("onDestroy Called")
    }

    override fun onRestart() {
        super.onRestart()
        Timber.i("onRestart Called")
    }
}
