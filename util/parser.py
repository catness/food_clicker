#!/usr/bin/python3
"""
Converts a .toml formatted file with the description of the layout
into Android Studio activity_main.xml ConstraintLayout.

cp activity_main.xml  ../app/src/main/res/layout/
cp activity_main-land.xml  ../app/src/main/res/layout-land/activity_main.xml
"""

import re, tomlkit

debug = False

layout_t = "" # read from template.xml

box_t = """
  <ImageView
        android:id="@+id/box_{num}"
        android:layout_width="0dp"
        android:layout_height="0dp"
        android:padding="@dimen/image_padding"
        android:scaleType="fitCenter" 
        app:srcCompat="@drawable/lollipop"
        {width}
        {height}
{edges} />

"""

height_pt = """
        app:layout_constraintHeight_default="percent"
        app:layout_constraintHeight_percent="{height}" 
"""
height_rt = """
        app:layout_constraintDimensionRatio="{height}" 
"""


width_pt = """
        app:layout_constraintWidth_default="percent"
        app:layout_constraintWidth_percent="{width}" 
"""
width_rt = """
        app:layout_constraintDimensionRatio="{width}" 
"""

edge_t = """        app:layout_constraint{src}_to{dst}Of="@+id/box_{num}" 
"""

edge_pt = """        app:layout_constraint{src}_to{src}Of="parent" 
"""
edge_g = """        app:layout_constraint{src}_to{dst}Of="@+id/guideline" 
"""


inverse = {"top":"bottom","bottom":"top","left":"right","right":"left"}


def make_box(box):
    height = box['height']
    if height == "spread":
        height_str = "app:layout_constraintHeight_default=\"spread\"\n"
    elif ':' in str(height):
        height_str = height_rt.format(height=height)
    else:
        height_str = height_pt.format(height=height)


    width = box['width']
    if width == "spread":
        witdh_str = "app:layout_constraintWidth_default=\"spread\"\n"
    elif ':' in str(width):
        width_str = width_rt.format(width=width)
    else:
        width_str = width_pt.format(width=width)

    edges = ""
    for edge in ["left","right","top","bottom"]:
        if edge in box:
            if box[edge] == "parent":
                edges += edge_pt.format(src=edge.capitalize())
            elif box[edge] == "guideline":
                edges += edge_g.format(src=edge.capitalize(),dst=inverse[edge].capitalize())
            else:
                dstnum,dst = box[edge].split(':')
                edges += edge_t.format(src=edge.capitalize(),dst=dst.capitalize(),num=dstnum)

    text = str(box['num']) if debug else ''
    out = box_t.format(num=box['num'],text=text,width=width_str,height=height_str,edges=edges)
    return out

filename = "template.xml"
with open(filename,"r") as f:
    layout_t = f.read()

filename = "layout.toml"
with open(filename,"r") as f:
    document = f.read()

y = tomlkit.parse(document)
views = ""
for box in y['box']:
    views += make_box(box)
layout = layout_t.format(views=views)

with open("activity_main.xml","w") as f:
    f.write(layout)


filename = "template-land.xml"
with open(filename,"r") as f:
    layout_t = f.read()

filename = "layout-land.toml"
with open(filename,"r") as f:
    document = f.read()

y = tomlkit.parse(document)
views = ""
for box in y['box']:
    views += make_box(box)
layout = layout_t.format(views=views)

with open("activity_main-land.xml","w") as f:
    f.write(layout)






